<!--
.. title: Simon Quigley
.. slug: index
.. date: 2019-01-01 06:00:00 UTC
.. tags: Simon,Quigley,SimonQuigley,main,page,mainpage,home,page,homepage
.. link:
.. description:
.. type: text
.. hidetitle: true
-->

This is Simon Quigley's personal website.

To learn more about Simon, see [About](/about/).

To donate to Simon, visit his [Patreon page](https://www.patreon.com/tsimonq2).
