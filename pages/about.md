<!--
.. title: About | Simon Quigley
.. slug: about
.. date: 2019-01-01 06:00:00 UTC
.. tags: Simon,Quigley,SimonQuigley,tsimonq2,about
.. link:
.. description: About Simon Quigley.
.. type: text
.. hidetitle: true
-->

Simon Quigley is a 16 year old Ubuntu and Debian community member.

## Ubuntu

 - Lubuntu Release Manager
 - Kubuntu Council Member
 - Ubuntu Core Developer
 - Ubuntu Developer Membership Board Member
 - Ubuntu Membership Board Member
 - Ubuntu Member, Lubuntu Member, Ubuntu Member
 - Primary Qt Maintainer in Ubuntu
 - Kubuntu Ninja
 - MOTU SWAT Team Member

## Debian

 - Debian Developer
 - Debian Qt/KDE Team Member

## Social Media

[Twitter](https://twitter.com/tsimonquigley2)
[Telegram](https://t.me/tsimonq2)
